//
//  MoviesResults+Decodable.swift
//  MovieSearcher
//
//  Created by Mikhail Pchelnikov on 10/06/2018.
//  Copyright © 2018 Michael Pchelnikov. All rights reserved.
//

import Foundation

/**
 Data structure for movies search results + Decodable implementation.
 */
extension MoviesResults: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.movies = try values.decode([Movie].self, forKey: .results)
    }
}
