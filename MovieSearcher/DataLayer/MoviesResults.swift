//
//  MoviesResults.swift
//  MovieSearcher
//
//  Created by Mikhail Pchelnikov on 10/06/2018.
//  Copyright © 2018 Michael Pchelnikov. All rights reserved.
//

import Foundation

/**
 Data structure for movies search results.
 */
struct MoviesResults {
    
    let movies: [Movie]
}
