## Movie Searcher

### Requirements

- iOS 9.0+
- Xcode 9.4+
- Swift 4.1+

### Third-party dependencies

- [RxSwift](https://github.com/ReactiveX/RxSwift) — Reactive Programming in Swift.
- [RxCocoa](https://github.com/ReactiveX/RxSwift/tree/master/RxCocoa) — Reactive Programming in Swift.
- [Kingfisher](https://github.com/onevcat/Kingfisher) — A lightweight, pure-Swift library for downloading and caching images from the web.